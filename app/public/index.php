<?php

require __DIR__ . '/../vendor/autoload.php';
$router = new \Bramus\Router\Router();

// add your routes and run!
$router->setNamespace('\Http');

$router->before('GET|POST', '/.*', function () {
    session_start();
});

$router->get('/', 'NewsController@overview');
$router->get('/resultscategory=(\d+)', 'NewsController@result');
$router->get('/articles/add', 'NewsController@showAddArticle');
$router->post('/articles/add', 'NewsController@addArticle');

$router->get('/login', 'AuthController@showLogin');
$router->post('/login', 'AuthController@login');
$router->get('/logout', 'AuthController@logout');


$router->run();