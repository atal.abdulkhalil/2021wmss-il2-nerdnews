<?php


namespace Http;


use Services\DatabaseConnector;

class AuthController
{
    protected \Doctrine\DBAL\Connection $db;
    protected \Twig\Environment $twig;

    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../../resources/templates');
        $this->twig = new \Twig\Environment($loader);

        $this->db = DatabaseConnector::getConnection();
    }


    public function showLogin()
    {
        $categories = $this->db->fetchAllAssociative('SELECT * FROM `categories`', []);
        //if already logged in
        if (isset($_SESSION['user'])) {
            header('Location: /');
            exit();
        }

        $formErrors = isset($_SESSION['flash']['formErrors']) ? $_SESSION['flash']['formErrors'] : [];
        $email = isset($_SESSION['flash']['email']) ? $_SESSION['flash']['email'] : '';
        unset($_SESSION['flash']);

        echo $this->twig->render('pages/login.twig', [
            'categories' => $categories,
            'email' => $email,
            'errors' => $formErrors
        ]);
    }


    public function login()
    {
        $formErrors = [];

        $email = isset($_POST['email']) ? trim($_POST['email']) : '';
        $password = isset($_POST['password']) ? trim($_POST['password']) : '';

        if (isset($_POST['moduleAction']) && ($_POST['moduleAction'] == 'login')){
            $stmt = $this->db->prepare('SELECT * FROM authors WHERE email = ?');
            $stmt->execute([$email]);
            $user = $stmt->fetchAssociative();

            if (($user !== false) && (password_verify($password, $user['password']))) {
                $_SESSION['user'] = $user;
                header('Location: /');
                exit();
            }
            else {
                $formErrors[] = 'invalid';
                $_SESSION['flash'] = ['formErrors' => $formErrors,
                    'email' => $email];
            }
        }
        header('Location: login');
        exit();
    }


    public function logout()
    {
        $_SESSION = [];

        if (ini_get('session.use_cookies')) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params['path'], $params['domain'],
                $params['secure'], $params['httponly']
            );
        }
        session_destroy();
        header('Location: /');
        exit();
    }
}