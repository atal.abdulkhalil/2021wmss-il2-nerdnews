<?php


namespace Http;

use Services\DatabaseConnector;

class NewsController
{
    protected \Doctrine\DBAL\Connection $db;
    protected \Twig\Environment $twig;


    public function __construct()
    {
        $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../../resources/templates');
        $this->twig = new \Twig\Environment($loader);

        $this->db = DatabaseConnector::getConnection();
    }



    public function overview()
    {
        $categories = $this->db->fetchAllAssociative('SELECT * FROM `categories`', []);
        $laatsteNews = $this->db->fetchAssociative('SELECT newsmessages.id, newsmessages.title, newsmessages.message, newsmessages.alt, DATE_FORMAT(newsmessages.pubdate, "%d-%m-%Y") AS pubdate, authors.firstname AS author FROM newsmessages LEFT JOIN authors on newsmessages.author_id = authors.id ORDER BY newsmessages.pubdate DESC LIMIT 1', []);
        $articles = $this->db->fetchAllAssociative('SELECT newsmessages.id, newsmessages.title, newsmessages.message, newsmessages.alt, DATE_FORMAT(newsmessages.pubdate, "%d-%m-%Y") AS pubdate, authors.firstname AS author FROM newsmessages LEFT JOIN authors on newsmessages.author_id = authors.id ORDER BY newsmessages.popularity DESC LIMIT 3', []);
        echo $this->twig->render('pages/index.twig', [
            'categories' => $categories,
            'laatsteNews' => $laatsteNews,
            'articles' => $articles,
        ]);
    }

    public function result($id)
    {
        $errors = '';
        $stmt = $this->db->prepare('SELECT * FROM `categories` where categories.id = ?');
        $stmt->execute([$id]);
        if ($stmt->rowCount() === 0) {
            $errors = 'true';
        }
        $categories = $this->db->fetchAllAssociative('SELECT * FROM `categories`', []);
        $articles = $this->db->fetchAllAssociative('SELECT newsmessages.id, newsmessages.title, newsmessages.message, newsmessages.alt, DATE_FORMAT(newsmessages.pubdate, "%d-%m-%Y") AS pubdate, authors.firstname AS author FROM newsmessages LEFT JOIN authors on newsmessages.author_id = authors.id WHERE newsmessages.category_id = 1', []);
        echo $this->twig->render('pages/result.twig', [
            'categories' => $categories,
            'articles' => $articles,
            'errors' => $errors
        ]);
    }

    public function showAddArticle()
    {
        if (! isset($_SESSION['user'])) {
            header('Location: /login');
            exit();
        }

        $errorList = isset($_SESSION['flash']['errorList']) ? $_SESSION['flash']['errorList'] : [];
        $title = isset($_SESSION['flash']['title']) ? $_SESSION['flash']['title'] : null;
        $alt = isset($_SESSION['flash']['alt']) ? $_SESSION['flash']['alt'] : null;
        $message = isset($_SESSION['flash']['message']) ? $_SESSION['flash']['message'] : null;
        $category = isset($_SESSION['flash']['category']) ? $_SESSION['flash']['category'] : 0;
        unset($_SESSION['flash']);
        $categories = $this->db->fetchAllAssociative('SELECT * FROM `categories`', []);

        echo $this->twig->render('pages/add.twig', [
            'categories' => $categories,
            'errors' => $errorList,
            'title' => $title,
            'alt' => $alt,
            'message' => $message,
            'selectedCategoryId' => $category
        ]);
    }

    public function addArticle()
    {
        $categories = $this->db->fetchAllAssociative('SELECT * FROM `categories`', []);

        $user = ($_SESSION['user']) ? $_SESSION['user'] : '';
        $title = (isset($_POST['title']) ? $_POST['title'] : '');
        $message = isset($_POST['message']) ? (string) $_POST['message'] : '';
        $category = (isset($_POST['category']) ? $_POST['category'] : 0);
        $alt = (isset($_POST['alt']) ? $_POST['alt'] : '');
        $moduleAction = (isset($_POST['moduleAction']) ? $_POST['moduleAction'] : '');

        $errorList = [];

        if ($moduleAction === 'add') {
            if (isset($_FILES['photo']) && ($_FILES['photo']['error'] === UPLOAD_ERR_OK)) {
                if (!in_array((new \SplFileInfo($_FILES['photo']['name']))->getExtension(), ['jpg'])) {
                    $errorList[] = 'Gelieve een foto up te loaden, enkel .JPG toegestaan';
                }
            }
            else {
                $errorList[] = 'Upload een foto';
            }
            if (strlen($title) < 3) {
                $errorList[] = ' Geef een title langere titel';
            }
            if (strlen($message) < 3) {
                $errorList[] = ' Geef een message langere titel';
            }
            if (strlen($alt) < 3) {
                $errorList[] = ' Geef een alt langer dan 3 karakers';
            }
            if (($category > $categories) || ($category === '0')) {
                $errorList[] = 'Kies een juiste category';
            }

            if (count($errorList) == 0) {
                $stmt = $this->db->prepare('INSERT INTO `newsmessages` (`title`, `message`, `alt`, `pubdate`, `category_id`, `author_id`, `popularity`) VALUES (?, ? , ? , ?, ?, ?, ?)');

                $stmt->execute([$title, $message, $alt, (new \DateTime())->format('Y-m-d H:i:s'), $category, $user['id'], 0]);
                $insertId = $this->db->lastInsertId();
                if ($insertId) {
                    if (!move_uploaded_file($_FILES['photo']['tmp_name'], __DIR__ . '/../../public/files/newsimages/' . 'news' . $insertId . '.' . (new \SplFileInfo($_FILES['photo']['name']))->getExtension())) {
                        $errorList[] = 'Kon de foto niet uploaden';
                    }
                }
                header('Location: /');
                exit();
            }
            $_SESSION['flash'] = ['errorList' => $errorList,
                'title' => $title,
                'message' => $message,
                'category' => $category,
                'alt' => $alt
            ];
            header('Location: /articles/add');
            exit();
        }
    }


}